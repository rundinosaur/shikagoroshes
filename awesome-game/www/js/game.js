/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Global variables / Constants
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
var game = null;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Global Game Manager
 * This object will be keeping track of all the flows.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
GlobalGameManager = function () {
	this.initialize.apply(this, arguments);
}
GlobalGameManager.prototype = {
    scoreManager: null,
    state: "",
	initialize: function () {
        var self = this;
        // Set this object as a global variable
        globalGameManager = this;
        game = new Phaser.Game(800, 1400, Phaser.CANVAS, 'gameDiv');

        // game.stage.scale.startFullScreen();
        this.scoreManager = new ScoreManager();
        this.bootManager = new BootManager({
            next: function () {
                self.menu();
            }
        });
        this.menuManager = new MenuManager({
            next: function (level) {
                self.menu2(level);
            }
        });
        this.menu2Manager = new Menu2Manager({
            next: function (level, team) {
                globalGameManager.menuManager.audio.stop();
                self.play(level, team);
            }
        });
        this.gameManager = new GameManager({
            over: function () {
                self.over();
            }
        });
        this.gameOverManager = new GameOverManager({
            next: function () {
                self.menu();
            }
        });
        this.boot();
        // /this.play(1, 2);
	},
	boot: function () {
        this.state = "boot";
        this.bootManager.start();
    },
    menu: function () {
        this.state = "menu";
        this.menuManager.start();
    },
    menu2: function (level) {
        this.state = "menu2";
        this.menu2Manager.start(level);
    },
    play: function (level, team) {
        this.state = "game";
        this.gameManager.start(level, team);
    },
    over: function () {
        this.state = "gameover";
        this.gameOverManager.start();
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Boot Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
BootManager = function () {
    this.initialize.apply(this, arguments);
}
BootManager.prototype = {
    initialize: function (options) {
        this.next = options.next;
        game.state.add('boot', this);
    },
    preload: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.setScreenSize(true);
        game.load.image('boot', 'assets/boot.png');
    },
    create: function() { 
        var self = this;
        game.add.sprite(0, 100, 'boot');
        setTimeout(function () {
            self.next();
        }, 1000);
    },
    update: function() {
    },
    start: function () {
        game.state.start('boot');
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Menu Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
MenuManager = function () {
    this.initialize.apply(this, arguments);
}
MenuManager.prototype = {
    initialize: function (options) {
        this.next = options.next;
        game.state.add('menu', this);
    },
    preload: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.setScreenSize(true);
        game.load.image('level1', 'assets/easy.png', 403, 141);
        game.load.image('level2', 'assets/medium.png', 617, 142);
        game.load.image('level3', 'assets/hard.png', 422, 142);
        game.load.audio('splash', 'audio/splash.mp3');
    },
    create: function() { 
        var self = this;
        this.audio = game.add.audio('splash', 1, true);
        this.audio.play("", 0, 1, true);

        this.level1 = game.add.button(game.world.centerX - 201, 460, 'level1', function () {
            self.next(1);
        }, this);
        this.level2 = game.add.button(game.world.centerX - 309, 660, 'level2', function () {
            self.next(2);
        }, this);
        this.level3 = game.add.button(game.world.centerX - 211, 860, 'level3', function () {
            self.next(3);
        }, this);
    },
    update: function() {
    },
    start: function () {
        game.state.start('menu');
    }
};

Menu2Manager = function () {
    this.initialize.apply(this, arguments);
}
Menu2Manager.prototype = {
    level: 1,
    initialize: function (options) {
        this.next = options.next;
        game.state.add('menu2', this);
    },
    preload: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.setScreenSize(true);

        game.load.image('team1', 'assets/blue.png', 410, 90);
        game.load.image('team2', 'assets/green.png', 410, 90);
        game.load.image('team3', 'assets/red.png', 410, 90);
        game.load.image('team4', 'assets/grey.png', 410, 90);
    },
    create: function() { 
        var self = this;
        this.team1 = game.add.button(game.world.centerX - 200, 460, 'team1', function () {
            self.next(self.level, 0);
        }, this);
        this.team1 = game.add.button(game.world.centerX - 200, 560, 'team2', function () {
            self.next(self.level, 3);
        }, this);
        this.team1 = game.add.button(game.world.centerX - 200, 660, 'team3', function () {
            self.next(self.level, 2);
        }, this);
        this.team1 = game.add.button(game.world.centerX - 200, 760, 'team4', function () {
            self.next(self.level, 1);
        }, this);
    },
    update: function() {
    },
    start: function (level) {
        this.level = level;
        game.state.start('menu2');
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Game Over Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
GameOverManager = function () {
    this.initialize.apply(this, arguments);
}
GameOverManager.prototype = {
    initialize: function (options) {
        this.next = options.next;
        game.state.add('over', this);
    },
    preload: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.setScreenSize(true);
        game.load.spritesheet('button', 'assets/play.png', 193, 71);
        game.load.image('gameoverBG', 'assets/gameover.png');
        game.load.audio('cheer', 'audio/gameover.mp3');
    },
    create: function() { 
        var self = this;

        this.bg = game.add.sprite(50, 400, 'gameoverBG');

        this.audioCheer = game.add.audio('cheer', 1);
        this.audioCheer.play("", 0, 1);

        globalGameManager.scoreManager.create();

        game.time.events.add(Phaser.Timer.SECOND * 3, function () {
            self.next();
        }, this).autoDestroy = true;
    },
    update: function() {
    },
    start: function () {
        game.state.start('over');
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Score Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
ScoreManager = function () {
    this.initialize.apply(this, arguments);
}
ScoreManager.prototype = {
    score: {
        1: 0,
        2: 0
    },
    tackles: {
        1: 0,
        2: 0
    },
    bg: null,
    defaultRemainTime: 60, // seconds
    remainTime: 0, // seconds
    oldTime: 0,
    shakeFrame: 0,
    shakeElm: [],
    shakeElmOrigin: [],
    initialize: function () {
    },
    preload: function() {
        game.load.image('scoreboard', 'assets/scoreboard.png');
        game.load.atlasJSONHash('number-b', 'assets/sprite_grid.png', 'assets/sprites/number-b.json');
        game.load.atlasJSONHash('number-s', 'assets/sprite_grid.png', 'assets/sprites/number-s.json');
    },
    create: function () {
        this.bg = game.add.sprite(0, 0, 'scoreboard');
        this.number1 = game.add.sprite(90, 100, 'number-b');
        this.number2 = game.add.sprite(140, 100, 'number-b');
        this.number3 = game.add.sprite(620, 100, 'number-b');
        this.number4 = game.add.sprite(670, 100, 'number-b');

        this.tackle1 = game.add.sprite(200, 150, 'number-s');
        this.tackle2 = game.add.sprite(230, 150, 'number-s');
        this.tackle3 = game.add.sprite(550, 150, 'number-s');
        this.tackle4 = game.add.sprite(580, 150, 'number-s');

        this.timeSec1 = game.add.sprite(350, 100, 'number-b');
        this.timeSec2 = game.add.sprite(400, 100, 'number-b');
        this.add(1,0);
        this.tackle(1,0);

    },
    shake: function (elements, frame) {
        this.shakeElm = elements;
        this.shakeFrame = frame;
        for (var i = elements.length - 1; i >= 0; i--) {
            this.shakeElmOrigin[i] = {x: elements[i].x, y: elements[i].y};
        };
    },
    add: function(player, score) {
        this.score[player] += score;
        // console.log("Score: ", this.score);
        this.updateNumber();

        if (player === 1) {
            this.shake([ this.number1, this.number2], 10);
        } else {
            this.shake([ this.number3, this.number4], 10);
        }
    },
    updateNumber: function () {
        this.number1.frame = parseInt(this.score[1] / 10);
        this.number2.frame = this.score[1] % 10;

        this.number3.frame = parseInt(this.score[2] / 10);
        this.number4.frame = this.score[2] % 10;
    },
    tackle: function(player, score) {
        this.tackles[player] += score;
        console.log("tackles: ", this.tackles);

        this.tackle1.frame = parseInt(this.tackles[1] / 10);
        this.tackle2.frame = this.tackles[1] % 10;
        this.tackle3.frame = parseInt(this.tackles[2] / 10);
        this.tackle4.frame = this.tackles[2] % 10;

        if (player === 1) {
            this.shake([ this.tackle1, this.tackle2], 10);
        } else {
            this.shake([ this.tackle3, this.tackle4], 10);
        }

    },
    reset: function() {
        this.score[1] = 0;
        this.score[2] = 0;
        this.tackles[1] = 0;
        this.tackles[2] = 0;
        this.remainTime = this.defaultRemainTime;
    },
    second: function () {
        this.remainTime--;
        this.timeSec1.frame = parseInt(this.remainTime / 10);
        this.timeSec2.frame = this.remainTime % 10;
        if (this.remainTime < 6) {
            this.shake([ this.timeSec1, this.timeSec2], 10);
        }
        if (this.remainTime === 0) {
            globalGameManager.gameManager.over();
        }
    },
    update:function () {
        if (this.shakeFrame > 0) {
           var rand1 = game.rnd.integerInRange(-10,10);
           var rand2 = game.rnd.integerInRange(-10,10);
           for (var i = this.shakeElm.length - 1; i >= 0; i--) {
               this.shakeElm[i].x = this.shakeElmOrigin[i].x + rand1; 
               this.shakeElm[i].y = this.shakeElmOrigin[i].y + rand2; 
           };
            this.shakeFrame--;
            if (this.shakeFrame === 0) {
                for (var i = this.shakeElm.length - 1; i >= 0; i--) {
                    this.shakeElm[i].x = this.shakeElmOrigin[i].x;
                    this.shakeElm[i].y = this.shakeElmOrigin[i].y;
                }
            }
        }
    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Game Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
GameManager = function () {
    this.initialize.apply(this, arguments);
}
GameManager.prototype = {
    /*
     * Member variables
     */
    // The areas in the field
    areas: {},
    playerTeams: {
            1: 0, // Player1's team
            2: 1 // Player2's team
    },
    players: {
        1: {},
        2: {}
    },
    oldTime: 0,
    swiping: false,
    selectedHero: null,
    movingHeroes: [],
    shakeWorld: 0,
    level: 2,
    /*
     * Constructor
     */
    initialize: function (options) {
        var self = this;
        this.play = options.play;
        this.over = function () {
            self.audio.stop();
            // self.audioCheer.stop();
            options.over();
        };
        game.state.add('game', this);
    },

    start: function (level, team) {
        console.log(level, team);
        this.level = level;
        this.playerTeams[1] = team;
        this.playerTeams[2] = team < 3 ? team + 1 : team - 1;
        game.state.start('game');
    },

    /*
     * Core preload function
     */
    preload: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.setScreenSize(true);
        // Load all our assets
        game.load.image('topGoal', 'assets/bg' + this.playerTeams[1] + '.png');
        game.load.image('whiteline', 'assets/whiteline.png');
        game.load.image('field', 'assets/bg_03.jpg');
        game.load.image('bottomGoal', 'assets/bg' + this.playerTeams[2] + '.png');

        // Load the hero sprites
        // TODO(Sota): Make this dynamic so that they chan change the team easilly
        for (var player = 1; player < 3; player++) {
            for (var kind = 0; kind < 3; kind++) {
                // Name convention: player[player kind]-[hero kind]-[action]-[direction]
                game.load.atlasJSONHash('player' + player + '-' + kind + '-run-back', 'assets/sprite_grid.png', 'assets/sprites/team-' + this.playerTeams[player] + '-' + kind + '-run-back.json');
                game.load.atlasJSONHash('player' + player + '-' + kind + '-run-front', 'assets/sprite_grid.png', 'assets/sprites/team-' + this.playerTeams[player] + '-' + kind + '-run-front.json');
                game.load.atlasJSONHash('player' + player + '-' + kind + '-defence-back', 'assets/sprite_grid.png', 'assets/sprites/team-' + this.playerTeams[player] + '-' + kind + '-defence-back.json');
                game.load.atlasJSONHash('player' + player + '-' + kind + '-defence-front', 'assets/sprite_grid.png', 'assets/sprites/team-' + this.playerTeams[player] + '-' + kind + '-defence-front.json');
                game.load.atlasJSONHash('player' + player + '-tackled', 'assets/sprite_grid.png', 'assets/sprites/team-' + this.playerTeams[player] + '-tackled.json');
            }
        }
        // Scoreboard 
        globalGameManager.scoreManager.preload();

        // Audio
        game.load.audio('grunt', 'audio/grunt.mp3');
        game.load.audio('game', 'audio/game.mp3');
        game.load.audio('score', 'audio/score.mp3');
        game.load.audio('score2', 'audio/score2.mp3');
    },

    /*
     * Core create function
     */
    create: function() { 
        var self = this;
        // Use Gesture to detect swiping
        this.gestures = new Gesture(game);

        // Audio
        this.audio = game.add.audio('game', 1, true);
        this.audio.play("", 0, 1, true);
        // this.audioCheer = game.add.audio('cheer', 1);
        // this.audioCheer.play("", 0, 1);
        this.audioScore = game.add.audio('score', 1);
        this.audioScore2 = game.add.audio('score2', 1);
        // this.audioGameOver.addMarker('gameover', 1, 5);
        // this.audioGameOver.play('gameover');

        // init areas
        this.areas = {
            0: {
                //lane
                lane: 0,
                // Speed
                velocity: 1,
                // Start location
                x: 0,
                y: 249,
                end_x: 0,
                end_y: 1074,
                defenceCount: 0
            },
            1: {
                //lane
                lane: 1,
                // Speed
                velocity: -1,
                // Start location
                x: 200,
                y: 1074,
                end_x: 200,
                end_y: 249,
                defenceCount: 0
            },
            2: {
                //lane
                lane: 2,
                // Speed
                velocity: 1,
                // Start location
                x: 400,
                y: 249,
                end_x: 400,
                end_y: 1074,
                defenceCount: 0
            },
            3: {
                //lane
                lane: 3,
                // Speed
                velocity: -1,
                // Start location
                x: 600,
                y: 1074,
                end_x: 600,
                end_y: 249,
                defenceCount: 0
            }
        };

        // Scoreboard 
        globalGameManager.scoreManager.create();
        globalGameManager.scoreManager.reset();

        // Add game objects
        game.add.sprite(0, 368, 'field');

        // Set goals
        this.players[1].goal = game.add.sprite(0, 249, 'topGoal');
        game.physics.arcade.enable(this.players[1].goal);
        this.players[1].goal.body.immovable = true;
        game.add.sprite(0, 368, 'whiteline');

        this.players[2].goal = game.add.sprite(0, 1274, 'bottomGoal');
        game.physics.arcade.enable(this.players[2].goal);
        this.players[2].goal.body.immovable = true;
        game.add.sprite(0, 1274, 'whiteline');

        // Create player1 group for running heros
        this.players[1].tackled = game.add.group();

        // Create player2 group for tackled heros
        this.players[2].tackled = game.add.group();

        // Create player2 group for defence heros
        this.players[2].defenceHeros = game.add.group();
        this.players[2].defenceHeros.enableBody = true;

        // Create player1 group for running heros
        this.players[1].heroes = game.add.group();
        this.players[1].heroes.enableBody = true;

        // Create player2 group for running heros
        this.players[2].heroes = game.add.group();
        this.players[2].heroes.enableBody = true;

        // Create player1 group for defence heros
        this.players[1].defenceHeros = game.add.group();
        this.players[1].defenceHeros.enableBody = true;

        // Add swipe detection
        this.gestures.onSwipe.add(function () {
            self.swipeHandler.apply(self, arguments);
        });
    },

    /*
     * Core Update function
     */
    update: function() {
        var self = this;
        // Handle collisions

        // Touch down for Player 1
        game.physics.arcade.collide(this.players[1].goal, this.players[1].heroes, function (goal, hero) {
            self.touchdown(1, goal, hero);
        }, null, this);

        // Defence for Player 1
        game.physics.arcade.collide(this.players[2].goal, this.players[1].heroes, function (goal, hero) {
            self.defence(1, goal, hero);
        }, null, this);

        // Touch down for Player 2
        game.physics.arcade.collide(this.players[2].goal, this.players[2].heroes, function (goal, hero) {
            self.touchdown(2, goal, hero);
        }, null, this);

        // Defence for Player 1
        game.physics.arcade.collide(this.players[1].goal, this.players[2].heroes, function (goal, hero) {
            self.defence(2, goal, hero);
        }, null, this);

        // Defence Collision when Player 1 is defending
        game.physics.arcade.collide(this.players[1].defenceHeros, this.players[2].heroes, function (defenceHero, hero) {
            self.defenceCollision(defenceHero, hero);
        }, null, this);

        // Defence Collision when Player 2 is defending
        game.physics.arcade.collide(this.players[2].defenceHeros, this.players[1].heroes, function (defenceHero, hero) {
            self.defenceCollision(defenceHero, hero);
        }, null, this);

        // Tackle Collision
        game.physics.arcade.collide(this.players[1].heroes, this.players[2].heroes, function (player1Hero, player2Hero) {
            self.tackle(player1Hero, player2Hero);
        }, null, this);

        // Swipe detection
        this.gestures.update();

        // Score board
        globalGameManager.scoreManager.update();
        
        // Time
        var now = (new Date()).getTime();
        if (Math.abs(now - this.oldTime) > 1000) {
            globalGameManager.scoreManager.second();
            self.spawnPlayerHero();
            this.oldTime = now;
        }

        if (this.shakeWorld > 0) {
           var rand1 = game.rnd.integerInRange(-20,20);
           var rand2 = game.rnd.integerInRange(-20,20);
            game.world.setBounds(rand1, rand2, game.width + rand1, game.height + rand2);
            this.shakeWorld--;
            if (this.shakeWorld == 0) {
                game.world.setBounds(0, 0, game.width,game.height); // normalize after shake?
            }
        }
    },

    /*
     * Utility funcitons
     */
    oldArea: 0,
    getRandomNumber: function (min, max) {
        var area = Math.floor((Math.random() * ((max + 1) * 10 -1))/10) + min;
        if (area === this.oldArea) {
            return this.getRandomNumber(min, max);
        }
        this.oldArea = area;
        return area;
    },

    spawnPlayerHero: function () {
        var areasToSpawn = {};
        var self = this;
        var i = 0;
        for (var player = 1; player < 3; player++) {
            var areaNumber = this.getRandomNumber(0,3);
            if (areasToSpawn[areaNumber]) continue;
            areasToSpawn[areaNumber] = true;
            // console.log(areaNumber);
            var area = this.areas[areaNumber];
            var options = {
                player: player,
                group: this.players[player].heroes,
                action: "run",
                direction: (area.velocity < 0) ? "back" : "front",
                heroKind: game.rnd.integerInRange(0,2), // TODO(Sota): make heroKind dynamic and random
                area: area,
                x: area.x,
                y: area.y
            };
            this.spawn(options, i);
            if (player === 2 && globalGameManager.gameManager.level === 3 || (globalGameManager.gameManager.level === 2 % game.rnd.integerInRange(0,10) % 2 === 0)) {
                // Spawn additional one
                this.spawn(options, i + 1);
            }
            i++;
        }
    },
    spawn: function (options, i) {
        var self = this;
        game.time.events.add(Phaser.Timer.SECOND * i * 0.5, function () {
            // Spawn the defence hero and add it to the hero
            var heroManager = new HeroManager(options);
            heroManager.enablePhysics();
            heroManager.addAnimation("run");
            heroManager.play("run");
            heroManager.onMousedown(function () {
                self.mouseDownHandler.apply(self, arguments);
            });
            heroManager.run();
        }, this).autoDestroy = true;

    },
    touchdown: function (player, goal, hero) {
        // console.log("touchdown. Player: " + player);
        globalGameManager.scoreManager.add(player, 1);
        if (hero._HeroManager.player === 1) {
            this.audioScore.play("", 0, 1);
        } else {
            this.audioScore2.play("", 0, 1);
        }
        hero._HeroManager.touchdown();
    },

    defence: function (player, goal, hero) {
        hero.animations.stop();
        var area = hero._HeroManager.area;
        console.log("area.defenceCount: ", player, area.defenceCount);
        if (area.defenceCount < 3) {
            // TODO(Sota): make heroKind dynamic and random
            var x = area.end_x + area.defenceCount * 15 - 15;
            var y = area.end_y + area.defenceCount * 15 - 15;
            var options = {
                player: player,
                group: this.players[player].defenceHeros,
                action: "defence",
                direction: (area.velocity < 0) ? "front" : "back",
                heroKind: hero._HeroManager.heroKind,
                area: area,
                x: x,
                y: y
            };
            // Spawn the defence hero and add it to the hero
            var defenceHeroManager = new HeroManager(options);
            defenceHeroManager.enablePhysics();
            defenceHeroManager.setImmovable(true);
            area.defenceCount++;
            console.log("Defence Added. Player: " + player);
            hero.kill();
        } else {
            if (hero._HeroManager.player === 2) {
                hero._HeroManager.moveToLeft();
            } else {
                hero.kill();
            }
        }
    },

    defenceCollision: function (defenceHero, hero) {
        var area = defenceHero._HeroManager.area;
        if (area.defenceCount > 0) {
            area.defenceCount--;
        }
        globalGameManager.scoreManager.tackle(defenceHero._HeroManager.player, 1);
        // defenceHero.kill();
        // hero.kill();
        // this.shakeWorld = 10;
        hero._HeroManager.defenceKill(defenceHero);
        // console.log("Defece workd.");
    },

    tackle: function (player1Hero, player2Hero) {
        globalGameManager.scoreManager.tackle(player1Hero._HeroManager.player, 1);
        player2Hero._HeroManager.tackled();
    },

    swipeHandler: function (gesture, pointA, pointB, swipeDirection) {
        // Only Player 1 can move their hero
        if (!this.selectedHero || this.selectedHero.player !== 1) {
            return false;
        }
        if (swipeDirection === Gesture.SwipeDirection.UP) {
            console.log("swipeHandler: Direction: UP");
        } else if (swipeDirection === Gesture.SwipeDirection.LEFT) {
            console.log("swipeHandler: Direction: LEFT");
            // Move to the right lane
            if (this.selectedHero.area.lane > 0) {
                this.selectedHero.moveToLeft();
            }
        } else if (swipeDirection === Gesture.SwipeDirection.DOWN) {
            console.log("swipeHandler: Direction: DOWN");
        } else if (swipeDirection === Gesture.SwipeDirection.RIGHT) {
            var self = this;
            console.log("swipeHandler: Direction: RIGHT");
            // Move to the right lane
            if (this.selectedHero.area.lane < 3) {
                this.selectedHero.moveToRight();
            }
        }
    },

    mouseDownHandler: function (hero, pointer) {
        var heroManager = hero._HeroManager;
        if (heroManager.player === 1) {
            this.selectedHero = heroManager;
            hero.bringToTop();
        } else {
            globalGameManager.scoreManager.tackle(1, 1);
            heroManager.tackled();
            this.shakeWorld = 10;
        }
        console.log("Mousedown", this, arguments);
    }
};


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Hero Manager
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
HeroManager = function () {
    this.initialize.apply(this, arguments);
}
HeroManager.prototype = {
    gameObj: null,
    group: null,
    area: null,
    heroKind: null,
    direction: null,
    player: null,
    heroFrame: 5,
    heroVelocity: 400,
    initialize: function (options) {
        this.player = options.player;   // 1 or 2
        this.heroKind = options.heroKind; // 0,1, or 2. It decides the charactor in the team
        this.direction = options.direction; // back or front
        this.group = options.group;         // Phaser group object
        this.area = options.area;
        this.action = options.action;

        if (globalGameManager.gameManager.level === 1) {
            this.heroVelocity = 300;
        } else if (globalGameManager.gameManager.level === 2) {
            this.heroVelocity = 500;
        } else if (globalGameManager.gameManager.level === 3) {
            this.heroVelocity = 600;
        } 

        var spriteName = this.getSpriteName(options.action);
        this.gameObj = game.add.sprite(options.x, options.y, spriteName, 0, this.group);
        this.gameObj._HeroManager = this;
        this.audioGrunt = game.add.audio('grunt');
        this.audioGrunt.addMarker('grunt', 0, 1.0);
    },
    moveToRight: function () {
        this.moveArea(1);
    },
    moveToLeft: function () {
        this.moveArea(-1);
    },
    bringToTop: function () {
        this.gameObj.group.bringToTop();
        game.world.bringToTop(this.gameObj);
    },
    moveArea: function (number) {
        var heroManager = this;
        var newArea = globalGameManager.gameManager.areas[heroManager.area.lane + number];
        heroManager.setVelocityY(0);
        var tween = game.add.tween(heroManager.gameObj);
        var options = {
            y: heroManager.pos().y,
            x: newArea.x
        };
        // console.log(options);
        tween.to(options, 500);
        tween.onComplete.add(function () {
            heroManager.area = newArea;
            heroManager.setDirection((newArea.velocity < 0) ? "back" : "front");
            heroManager.run();
            console.log("move finished");
        });
        tween.start();
    },
    setDirection: function (direction) {
        this.direction = direction; // back or front
        var spriteName = this.getSpriteName(this.action);
        var newGameObj = game.add.sprite(this.pos().x, this.pos().y, spriteName, 0, this.group);
        newGameObj._HeroManager = this;
        this.gameObj.kill();
        this.gameObj = newGameObj;
        this.addAnimation();
        this.play();
    },
    addAnimation: function () {
        this.gameObj.animations.add(this.action);
    },
    play: function () {
        this.gameObj.animations.play(this.action, this.heroFrame, true);
    },
    getSpriteName: function (action) {
        var spriteName = 'player' + this.player + '-' + this.heroKind + '-' + this.action + '-' + this.direction;
        return spriteName;
    },
    setImmovable: function (bool) {
        this.gameObj.body.immovable = bool;
    },
    setVelocityX: function (velocity) {
        this.gameObj.body.velocity.x = velocity * this.heroVelocity;
    },
    setVelocityY: function (velocity) {
        this.gameObj.body.velocity.y = velocity * this.heroVelocity;
    },
    pos: function () {
        return {x: this.gameObj.x, y: this.gameObj.y};
    },
    run: function (velocity) {
        this.gameObj.body.velocity.y = this.area.velocity * this.heroVelocity;
    },
    enablePhysics: function () {
        game.physics.arcade.enable(this.gameObj);
    },
    setInputEnabled: function (bool) {
       this.gameObj.inputEnabled = bool;
    },
    onMousedown: function (callback, scope) {
        this.setInputEnabled(true);
        this.gameObj.events.onInputDown.add(callback, scope);
    },
    tackled: function () {
        var group = globalGameManager.gameManager.players[this.player].tackled;
        var tackled = game.add.sprite(this.gameObj.x, this.gameObj.y, 'player' + this.player + '-tackled', 0, group);
        this.audioGrunt.play('grunt');
        game.time.events.add(Phaser.Timer.SECOND * 1, function () {
            tackled.kill();
        }, this).autoDestroy = true;
        this.gameObj.kill();
    },
    touchdown: function () {
        var self = this;
        var tween = game.add.tween(this.gameObj);
        var y = self.gameObj.y;
        if (self.direction === "back") {
            y -= 300;
        } else {
            y += 180;
        }
        var options = {
            y: y,
            x: self.gameObj.x
        };
        // console.log(options);
        tween.to(options, 400);
        tween.onComplete.add(function () {
            self.gameObj.kill();
        });
        tween.start();
    },
    defenceKill: function (defenceHero) {
        var self = this;
        var tween = game.add.tween(this.gameObj);
        var y = self.gameObj.y;
        if (self.direction === "back") {
            y -= 130;
        } else {
            y += 130;
        }
        var options = {
            y: y,
            x: self.gameObj.x
        };
        // console.log(options);
        tween.to(options, 200);
        tween.onComplete.add(function () {
            defenceHero.kill();
            self.tackled();
        });
        tween.start();
    },
    update: function () {

    }
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Gesture Helper class
 * https://github.com/eguneys/lose-your-marbles/blob/master/app/scripts/util/gestures.js
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
function Gesture(game) {
    this.game = game;
    
    this.swipeDispatched = false;
    this.holdDispatched = false;
    
    this.isTouching = false;
    this.isHolding = false;
    
    this.onSwipe = new Phaser.Signal();
    this.onTap = new Phaser.Signal();
    this.onHold = new Phaser.Signal();
}

Gesture.prototype.update = function() {
    var distance = Phaser.Point.distance(this.game.input.activePointer.position, this.game.input.activePointer.positionDown);
    var duration = this.game.input.activePointer.duration;

    this.updateSwipe(distance, duration);
    this.updateTouch(distance, duration);
};

Gesture.prototype.updateSwipe = function(distance, duration) {
    var velocity = distance / duration;
    if (velocity < 0) {
        this.swipeDispatched = false;
    } else if (!this.swipeDispatched && distance > Gesture.SwipeDefaults.threshold &&  velocity > Gesture.SwipeDefaults.velocity) {
        var positionDown = this.game.input.activePointer.positionDown;
        var position = this.game.input.activePointer.position;
        var swapDirection = this.getSwipeDirection(position, positionDown);
        
        this.onSwipe.dispatch(this, positionDown, position, swapDirection);

        this.swipeDispatched = true;
    }
};

Gesture.prototype.updateTouch = function(distance, duration) {
    var positionDown = this.game.input.activePointer.positionDown;

    if (duration < 0) {
        if (this.isTouching) {
            this.onTap.dispatch(this, positionDown);
        }

        this.isTouching = false;
        this.isHolding = false;
        this.holdDispatched = false;
        
    } else if (distance < Gesture.SwipeDefaults.threshold) {
        if (duration < Gesture.HoldDefaults.time) {
            this.isTouching = true;
        } else {
            this.isTouching = false;
            this.isHolding = true;

            if (!this.holdDispatched) {
                this.holdDispatched = true;
                
                this.onHold.dispatch(this, positionDown);
            }
        }
    } else {
        this.isTouching = false;
        this.isHolding = false;
    }
};

Gesture.prototype.getSwipeDirection = function(pos, prevPos) {
    var xDiff = pos.x - prevPos.x;
    var yDiff = pos.y - prevPos.y;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
        if (xDiff < 0) {
            return Gesture.SwipeDirection.LEFT;
        } else {
            return Gesture.SwipeDirection.RIGHT;
        }
    } else {
        if (yDiff < 0) {
            return Gesture.SwipeDirection.UP;
        } else {
            return Gesture.SwipeDirection.DOWN;
        }
    }
};

Gesture.SWIPE = 0;
Gesture.TAP = 1;
Gesture.HOLD = 2;

Gesture.HoldDefaults = {
    time: 200
};

Gesture.SwipeDefaults = {
    velocity: 0.65,
    threshold: 10
};

Gesture.SwipeDirection = {
    UP: 1,
    DOWN: 2,
    LEFT: 3,
    RIGHT: 4
    };
